﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMath
{
    class Program
    {
        static int myOp = 0;
        static int operand_1;
        static int operand_2;
        static int result = 0;
        static string chk = "Y";
        static void Main(string[] args)
        {
            do
            {
                do
                {
                    retry:
                    Console.WriteLine("Please select an opertion from the below options: ");
                    Console.WriteLine("1-Add\n2-Subtract\n3-Multiply\n4-Divide\n");
                    try
                    {
                        myOp = int.Parse(Console.ReadLine());

                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("\nInvalid Format \n");
                        goto retry;
                    }
                } while ((myOp != 1) && (myOp != 2) && (myOp != 3) && (myOp != 4));

                Console.WriteLine("Please enter the operand 1");
                operand_1 = int.Parse(Console.ReadLine());
                Console.WriteLine("Please enter the operand 2");
                operand_2 = int.Parse(Console.ReadLine());

                if (myOp == 1)
                {
                    result = operand_1 + operand_2;

                }
                else if (myOp == 2)
                {
                    result = Math.Abs(operand_1 - operand_2);
                }
                else if (myOp == 3)
                {
                    result = operand_1 * operand_2;
                }
                else if (myOp == 4)
                {
                    result = operand_1 / operand_2;
                }
                else
                {
                    Console.WriteLine("Wrong operation chosen /n");
                }
                Console.WriteLine("The result is " + result);
                Console.WriteLine("Do you want to do more operation?");
                chk = Console.ReadLine();
                Console.WriteLine("\n");
            } while ((chk == "Y") | (chk == "y"));
        }
    }
}